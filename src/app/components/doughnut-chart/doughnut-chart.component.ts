import { Component, OnInit } from '@angular/core';

import { ChartOptions, ChartType } from 'chart.js';
import { Label, SingleDataSet, PluginServiceGlobalRegistrationAndOptions } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

interface IChartValue {
  amount: number;
  label: string;
  backcroundColor: string;
}

@Component({
  selector: 'app-doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.css']
})
export class DoughnutChartComponent implements OnInit {
  private primerValor = 0;
  public segundoValor = 0;
  private tercerValor = 0;
  private cuartoValor = 0;

  private existSegundoValor = true;

  public chartLabels: Label[] = [];
  public chartData: SingleDataSet = [];
  public chartColors = [{ backgroundColor: [] }];
  public chartType: ChartType = 'doughnut';

  public chartPlugins: PluginServiceGlobalRegistrationAndOptions[] = [{
    afterDraw(chart) {
      const ctx = chart.ctx;

      // Calculamos total y damos formato peso Chileno
      const dataArr = chart.data.datasets[0].data as number[];
      const sum: number = dataArr.reduce((acc, item) => acc + item, 0);
      const totalAmount = new Intl.NumberFormat('es-CL', { style: 'currency', currency: 'CLP', minimumFractionDigits: 0 }).format(sum);

      // Alineamos texto en centro
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      // Hacemos cálculos para mantener el contenido bien centrado
      const chartArea = chart.chartArea;
      const centerX = ((chartArea.left + chartArea.right) / 2);
      const centerY = ((chartArea.top + chartArea.bottom) / 2);

      // Dibujar texto en centro de gráfico
      let fontSize = 10;
      let extraCenterY = 0;

      if (chartArea.right >= 520) {
        fontSize = 23;
        extraCenterY = 10;
      } else if (chartArea.right >= 370 && chartArea.right < 520) {
        fontSize = 15;
        extraCenterY = 5;
      }

      ctx.font = `${fontSize}px Arial`;
      ctx.fillText(totalAmount, centerX, centerY + extraCenterY);
    }
  }];
  chartPlugins2 = [pluginDataLabels];
  public optionsChart: ChartOptions = {
    cutoutPercentage: 65, // Grosor de gráfico de dona
    elements: {
      arc: { borderWidth: 0 }
    },
    hover: { mode: null }, // Al dejar en null el 'mode' este no hace nada al pasar mouse por arriba
    layout: { padding: 35 },
    legend: { display: false },
    maintainAspectRatio: true, // Mantener el aspecto según el espacio
    responsive: true,
    tooltips: { enabled: false }, // 'enabled' en false es para no mostrar información al pasar sobre un color
    plugins: {
      datalabels: {
        formatter: (value: number, ctx) => {
          // Label del valor
          const label = ctx.chart.data.labels[ctx.dataIndex];
          // Array con valores
          const dataArr = ctx.chart.data.datasets[0].data as number[];
          // Suma de todos los valores
          const sum: number = dataArr.reduce((acc, item) => acc + item, 0);
          // Porcentaje del valor según el total
          const percentage = (value * 100 / sum).toFixed(2);
          // Texto label
          return `${label}\n${percentage}%`;
        },
        color: '#000',
        align: 'end',
        anchor: 'end',
        clamp: true,
      }
    }
  }

  constructor() { }

  ngOnInit(): void {
    this.setRandomData();
  }

  resetDataChart(): void {
    this.chartData = [];
    this.chartLabels = [];
    this.chartColors[0].backgroundColor = [];
  }

  loadDataChart(): void {
    this.resetDataChart();

    for (let index = 0; index < 4; index++) {
      const investment = this.selectInvestment(index);
      if (investment.amount > 0) {
        this.chartData.push(investment.amount);
        this.chartLabels.push(investment.label);
        this.chartColors[0].backgroundColor.push(investment.backcroundColor);
      }
    }
  }

  selectInvestment(index: number): IChartValue {
    const defaultData = { amount: 0, label: '', backcroundColor: '' };
    switch (index) {
      case 0:
        return this.primerValor > 0 ? this.prepareValue(this.primerValor, 'Primer valor', '#E21B3C') : defaultData;
      case 1:
        return this.segundoValor > 0 ? this.prepareValue(this.segundoValor, 'Segundo valor', '#04A346') : defaultData;
      case 2:
        return this.tercerValor > 0 ? this.prepareValue(this.tercerValor, 'Tercer valor', '#D9D604') : defaultData;
      case 3:
        return this.cuartoValor > 0 ? this.prepareValue(this.cuartoValor, 'Cuarto valor', '#041ED9') : defaultData;
      default:
        return defaultData;
    }
  }

  prepareValue(amount: number, label: string, backcroundColor: string): IChartValue {
    return { amount, label, backcroundColor };
  }

  setRandomData(): void {
    this.primerValor = this.randomNumber();
    this.segundoValor = this.existSegundoValor ? this.randomNumber() : 0;
    this.tercerValor = this.randomNumber();
    this.cuartoValor = this.randomNumber();

    this.loadDataChart();
  }

  randomNumber(): number {
    return Math.floor(Math.random() * (5000000 - 20000) + 20000);
  }

  removeSecondValue(): void {
    if (this.segundoValor > 0) {
      this.existSegundoValor = false;
      this.segundoValor = 0;
      this.loadDataChart();
    }
  }

  addSecondValue(): void {
    if (this.segundoValor === 0) {
      this.existSegundoValor = true;
      this.segundoValor = this.randomNumber();
      this.loadDataChart();
    }
  }
}

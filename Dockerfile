FROM node:14-slim AS builder

WORKDIR /app

COPY package*.json ./

RUN npm install --no-optional

RUN npm install -g @angular/cli@12.0.2

COPY . .

RUN ng build --configuration=production

# NGINX
FROM nginx:alpine

COPY --from=builder /app/dist/graficos-angular /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
